# Watson OpenScale

## Demo Notebook

https://github.ibm.com/watson-developer-cloud/openscale-python-sdk/blob/master/sample_notebooks/Watson%20OpenScale%20V2%20and%20WML%20V4%20(IAM%20Token)%20(Scikit)%20.ipynb

## Requirements for monitoring

- Models must have **Column Names** specified, this can be done by specifing the `training_data=` argument in the [`store_model()`](http://ibm-wml-api-pyclient.mybluemix.net/#client.Repository.store_model) of `wml`.
- For the **1-hour** Quality & Bias check, and the **3-hour** Drift check, the model has to be marked as **Production**, i.e. come from a *Machine Learning Provider* which has the environment type set as **Production**.

## Check Payload Count & Payload data

Say that we have a authenticated Python Watson OpenScale Client, e.g;
```
from ibm_cloud_sdk_core.authenticators import IAMAuthenticator

from ibm_watson_openscale import *
from ibm_watson_openscale.supporting_classes.enums import *
from ibm_watson_openscale.supporting_classes import *


authenticator = IAMAuthenticator(apikey=CLOUD_API_KEY)
wos_client = APIClient(authenticator=authenticator)
```

Find the **id** of a `payload_logging` subscription with

```
wos_client.data_sets.show()
```

We can get the payload count with;

```
import uuid
import time

from ibm_watson_openscale.supporting_classes.payload_record import PayloadRecord

pl_records_count = wos_client.data_sets.get_records_count("9031d927-0b0c-4fbe-b42e-a16f1e8cae68")
print("Number of records in the payload logging table: {}".format(pl_records_count))
```

We can see the actual payload with;

```
wos_payload = wos_client.data_sets.get_list_of_records("9031d927-0b0c-4fbe-b42e-a16f1e8cae68")
wos_payload.get_result()

```


## Automating Payload Logging

[Automated Payload Logging](https://cloud.ibm.com/docs/ai-openscale?topic=ai-openscale-fmrk-workaround-pyld-lg) between IBM® Watson OpenScale and IBM Watson™ Machine Learning is possible when they are provisioned in the same IBM Cloud account (or for IBM® Watson OpenScale is on the same IBM® Cloud Pak for Data cluster).

This will make sure all API Scoring Requests to the model are automatically logged & processed by OpenScale, for instance needed to detect Bias.


## Trigger Information

Quality, Fairness, & Drift all have individual **sample size** requirement, the minimum number of transactions needed for a check to be performed at all. 

| Evaluations		| Requirement			| Triggered by... 	|
|-------------------|-----------------------|-------------------|
| Fairness			| Sufficient API Calls to monitored Model (& Training Data) | Refreshed [Every hour](https://cloud.ibm.com/docs/ai-openscale?topic=ai-openscale-anlz_metrics_fairness), or manually triggered	|
| Quality			| Evaluation Data		| [Every hour](https://cloud.ibm.com/docs/ai-openscale?topic=ai-openscale-anlz_metrics), or manually triggered	|
| Drift				| Sufficient API Calls to monitored Model (& Training Data) | Every 3 hours	|
| Explainability	|  API Calls to monitored Model (& Training Data) | -	|